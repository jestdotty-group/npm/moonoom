const Query = require('./query')
const Entity = require('./entity')

class Engine{
	constructor(){
		this.components = new Map()
		this.systems = new Map()
	}
	update(category, ...args){
		this.systems.get(category).forEach(s=> s(this, ...args))
	}
	//systems
	add(category, ...systems){
		if(this.systems.has(category)) throw new Error(`System category "${category}" already exists`)
		this.systems.set(category, [...systems])
	}
	//components
	_add(entity, ...componentNames){
		for(const name of componentNames){
			if(this.components.has(name)) this.components.get(name).add(entity)
			else this.components.set(name, new Set([entity]))
		}
	}
	_remove(entity, force, ...componentNames){
		for(const name of componentNames){
			if(!force && entity.has(name)) continue //if still has a component by this name do nothing
			const set = this.components.get(name)
			if(!set) return
			set.delete(entity)
			if(!set.size) this.components.delete(name) //if no more of a component remove artifacts
		}
	}
	//queries
	query(query){return query.search(this)}
	search(include, exclude){return (new Query(include, exclude)).search(this)}
	get(...componentQueries){return this.search(componentQueries)}
	//laziness
	create(...components){return Entity.create(...components).attach(this)}
}

module.exports = Engine
