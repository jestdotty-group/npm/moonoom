const path = require('path')
const pkg = require('./package')

module.exports = {
	mode: 'none',
	entry: path.join(__dirname, pkg.main),
	output: {
		path: path.join(__dirname, 'dist'),
		filename: `${pkg.name}.js`,
		library: pkg.name
	},
	devtool: "source-map"
}
