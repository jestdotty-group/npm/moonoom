const assert = require('assert')

const Components = require('../src/components')

class Component{}
class AnotherComponent{}
class ComponentIs{[Components.IS](c){return c===this}}

describe('components', ()=>{
	describe('create', ()=>{
		it('empty', ()=>{
			const components = new Components()
			assert.equal(components.size, 0)
		})
		it('one', ()=>{
			const component = new Component()
			const components = new Components(component)

			assert.equal(components.size, 1)
			assert.equal(components.get('component'), component)
		})
		it('two', ()=>{
			const component = new Component()
			const anotherComponent = new AnotherComponent()
			const components = new Components(component, anotherComponent)

			assert.equal(components.size, 2)
			assert.equal(components.get('component'), component)
			assert.equal(components.get('anotherComponent'), anotherComponent)
		})
		it('duplicate', ()=>{
			const component1 = new Component()
			const component2 = new Component()
			const components = new Components(component1, component2)

			assert.equal(components.size, 1)
			assert.deepEqual(components.get('component'), [
				component1,
				component2
			])
		})
	})
	describe('add', ()=>{
		it('empty', ()=>{
			const components = new Components()
			assert.equal(components.size, 0)

			const added = components.add()
			assert.deepEqual(added, [])
			assert.equal(components.size, 0)
		})
		it('one', ()=>{
			const components = new Components()
			assert.equal(components.size, 0)

			const component = new Component()
			const added = components.add(component)
			assert.deepEqual(added, ['component'])
			assert.equal(components.size, 1)
			assert.equal(components.get('component'), component)
		})
		it('two', ()=>{
			const components = new Components()
			assert.equal(components.size, 0)

			const component = new Component()
			const anotherComponent = new AnotherComponent()
			const added = components.add(component, anotherComponent)
			assert.deepEqual(added, ['component', 'anotherComponent'])
			assert.equal(components.size, 2)
			assert.equal(components.get('component'), component)
			assert.equal(components.get('anotherComponent'), anotherComponent)
		})
		it('duplicate', ()=>{
			const components = new Components()
			assert.equal(components.size, 0)

			const component1 = new Component()
			const component2 = new Component()
			const added = components.add(component1, component2)
			assert.deepEqual(added, ['component', 'component'])
			assert.equal(components.size, 1)
			assert.deepEqual(components.get('component'), [
				component1,
				component2
			])

			const component3 = new Component()
			const added2 = components.add(component3)
			assert.deepEqual(added2, ['component'])
			assert.equal(components.size, 1)
			assert.deepEqual(components.get('component'), [
				component1,
				component2,
				component3
			])
		})
	})
	describe('remove', ()=>{
		it('empty', ()=>{
			const components = new Components()
			assert.equal(components.size, 0)

			const removed = components.remove()
			assert.deepEqual(removed, [])
			assert.equal(components.size, 0)
		})
		describe('string', ()=>{
			it('non existent of empty', ()=>{
				const components = new Components()
				assert.equal(components.size, 0)

				const removed = components.remove('component')
				assert.deepEqual(removed, [])
				assert.equal(components.size, 0)
			})
			it('non existent of one', ()=>{
				const component = new Component()
				const components = new Components(component)
				assert.equal(components.size, 1)
				assert.equal(components.get('component'), component)

				const removed = components.remove('componentz')
				assert.deepEqual(removed, [])
				assert.equal(components.size, 1)
				assert.equal(components.get('component'), component)
			})
			it('non existent of two duplicates', ()=>{
				const component1 = new Component()
				const component2 = new Component()
				const components = new Components(component1, component2)

				assert.equal(components.size, 1)
				assert.deepEqual(components.get('component'), [
					component1,
					component2
				])

				const removed = components.remove('componentz')
				assert.deepEqual(removed, [])
				assert.equal(components.size, 1)
				assert.deepEqual(components.get('component'), [
					component1,
					component2
				])
			})
			it('one of one', ()=>{
				const component = new Component()
				const components = new Components(component)
				assert.equal(components.size, 1)
				assert.equal(components.get('component'), component)

				const removed = components.remove('component')
				assert.deepEqual(removed, ['component'])
				assert.equal(components.size, 0)
				assert.equal(components.get('component'), undefined)
			})
			it('one of two', ()=>{
				const component = new Component()
				const anotherComponent = new AnotherComponent()
				const components = new Components(component, anotherComponent)
				assert.equal(components.size, 2)
				assert.equal(components.get('component'), component)
				assert.equal(components.get('anotherComponent'), anotherComponent)

				const removed = components.remove('component')
				assert.deepEqual(removed, ['component'])
				assert.equal(components.size, 1)
				assert.equal(components.get('component'), undefined)
				assert.equal(components.get('anotherComponent'), anotherComponent)
			})
			it('two of two', ()=>{
				const component = new Component()
				const anotherComponent = new AnotherComponent()
				const components = new Components(component, anotherComponent)
				assert.equal(components.size, 2)
				assert.equal(components.get('component'), component)
				assert.equal(components.get('anotherComponent'), anotherComponent)

				const removed = components.remove('component', 'anotherComponent')
				assert.deepEqual(removed, ['component', 'anotherComponent'])
				assert.equal(components.size, 0)
				assert.equal(components.get('component'), undefined)
				assert.equal(components.get('anotherComponent'), undefined)
			})
			it('duplicates', ()=>{
				const component1 = new Component()
				const component2 = new Component()
				const component3 = new Component()
				const components = new Components(component1, component2, component3)

				assert.equal(components.size, 1)
				assert.deepEqual(components.get('component'), [
					component1,
					component2,
					component3
				])

				const removed = components.remove('component')
				assert.deepEqual(removed, ['component'])
				assert.equal(components.size, 1)
				assert.deepEqual(components.get('component'), [component2, component3])

				const removed2 = components.remove('component')
				assert.deepEqual(removed2, ['component'])
				assert.equal(components.size, 1)
				assert.deepEqual(components.get('component'), component3)
			})
		})
		describe('string$', ()=>{
			it('non existent of empty', ()=>{
				const components = new Components()
				assert.equal(components.size, 0)

				const removed = components.remove('component$')
				assert.deepEqual(removed, [])
				assert.equal(components.size, 0)
			})
			it('non existent of one', ()=>{
				const component = new Component()
				const components = new Components(component)
				assert.equal(components.size, 1)
				assert.equal(components.get('component'), component)

				const removed = components.remove('componentz$')
				assert.deepEqual(removed, [])
				assert.equal(components.size, 1)
				assert.equal(components.get('component'), component)
			})
			it('non existent of two duplicates', ()=>{
				const component1 = new Component()
				const component2 = new Component()
				const components = new Components(component1, component2)

				assert.equal(components.size, 1)
				assert.deepEqual(components.get('component'), [
					component1,
					component2
				])

				const removed = components.remove('componentz$')
				assert.deepEqual(removed, [])
				assert.equal(components.size, 1)
				assert.deepEqual(components.get('component'), [
					component1,
					component2
				])
			})
			it('one of one', ()=>{
				const component = new Component()
				const components = new Components(component)
				assert.equal(components.size, 1)
				assert.equal(components.get('component'), component)

				const removed = components.remove('component$')
				assert.deepEqual(removed, ['component'])
				assert.equal(components.size, 0)
				assert.equal(components.get('component'), undefined)
			})
			it('duplicates', ()=>{
				const component1 = new Component()
				const component2 = new Component()
				const components = new Components(component1, component2)

				assert.equal(components.size, 1)
				assert.deepEqual(components.get('component'), [
					component1,
					component2
				])

				const removed = components.remove('component$')
				assert.deepEqual(removed, ['component', 'component'])
				assert.equal(components.size, 0)
				assert.deepEqual(components.get('component'), undefined)
			})
		})
		describe('instance', ()=>{
			it('non existent of one', ()=>{
				const component1 = new Component()
				const component2 = new Component()
				const components = new Components(component1)
				assert.equal(components.size, 1)
				assert.equal(components.get('component'), component1)

				const removed = components.remove(component2)
				assert.deepEqual(removed, [])
				assert.equal(components.size, 1)
				assert.equal(components.get('component'), component1)
			})
			it('different name of one', ()=>{
				const component = new Component()
				const anotherComponent = new AnotherComponent()
				const components = new Components(component)
				assert.equal(components.size, 1)
				assert.equal(components.get('component'), component)

				const removed = components.remove(anotherComponent)
				assert.deepEqual(removed, [])
				assert.equal(components.size, 1)
				assert.equal(components.get('component'), component)
			})
			it('non existent of two duplicates', ()=>{
				const component1 = new Component()
				const component2 = new Component()
				const component3 = new Component()
				const components = new Components(component1, component2)

				assert.equal(components.size, 1)
				assert.deepEqual(components.get('component'), [
					component1,
					component2
				])

				const removed = components.remove(component3)
				assert.deepEqual(removed, [])
				assert.equal(components.size, 1)
				assert.deepEqual(components.get('component'), [
					component1,
					component2
				])
			})
			it('non existent of three duplicates', ()=>{
				const component1 = new Component()
				const component2 = new Component()
				const component3 = new Component()
				const component4 = new Component()
				const components = new Components(component1, component2, component3)

				assert.equal(components.size, 1)
				assert.deepEqual(components.get('component'), [
					component1,
					component2,
					component3
				])

				const removed = components.remove(component4)
				assert.deepEqual(removed, [])
				assert.equal(components.size, 1)
				assert.deepEqual(components.get('component'), [
					component1,
					component2,
					component3
				])
			})
			it('one of one', ()=>{
				const component = new Component()
				const components = new Components(component)
				assert.equal(components.size, 1)
				assert.equal(components.get('component'), component)

				const removed = components.remove(component)
				assert.deepEqual(removed, ['component'])
				assert.equal(components.size, 0)
				assert.equal(components.get('component'), undefined)
			})
			it('one of two duplicates', ()=>{
				const component1 = new Component()
				const component2 = new Component()
				const components = new Components(component1, component2)

				assert.equal(components.size, 1)
				assert.deepEqual(components.get('component'), [
					component1,
					component2
				])

				const removed = components.remove(component1)
				assert.deepEqual(removed, ['component'])
				assert.equal(components.size, 1)
				assert.deepEqual(components.get('component'), component2)
			})
			it('one of two duplicates IS', ()=>{
				const component1 = new ComponentIs()
				const component2 = new ComponentIs()
				const components = new Components(component1, component2)

				assert.equal(components.size, 1)
				assert.deepEqual(components.get('componentIs'), [
					component1,
					component2
				])

				const removed = components.remove(component1)
				assert.deepEqual(removed, ['componentIs'])
				assert.equal(components.size, 1)
				assert.deepEqual(components.get('componentIs'), component2)
			})
			it('two of two duplicates', ()=>{
				const component1 = new Component()
				const component2 = new Component()
				const components = new Components(component1, component2)

				assert.equal(components.size, 1)
				assert.deepEqual(components.get('component'), [
					component1,
					component2
				])

				const removed = components.remove(component1, component2)
				assert.deepEqual(removed, ['component', 'component'])
				assert.equal(components.size, 0)
				assert.deepEqual(components.get('component'), undefined)
			})
			it('three of three duplicates', ()=>{
				const component1 = new Component()
				const component2 = new Component()
				const component3 = new Component()
				const components = new Components(component1, component2, component3)

				assert.equal(components.size, 1)
				assert.deepEqual(components.get('component'), [
					component1,
					component2,
					component3
				])

				const removed = components.remove(component1, component2)
				assert.deepEqual(removed, ['component', 'component'])
				assert.equal(components.size, 1)
				assert.deepEqual(components.get('component'), component3)

				const removed2 = components.remove(component3)
				assert.deepEqual(removed2, ['component'])
				assert.equal(components.size, 0)
				assert.deepEqual(components.get('component'), undefined)
			})
		})
	})
	describe('getComplex', ()=>{
		it('get non existent', ()=>{
			const components = new Components()
			assert.equal(components.size, 0)

			const gotten = components.getComplex('component')
			assert.deepEqual(gotten, undefined)
		})
		it('get one of one', ()=>{
			const component = new Component()
			const components = new Components(component)
			assert.equal(components.size, 1)
			assert.deepEqual(components.get('component'), component)

			const gotten = components.getComplex('component')
			assert.deepEqual(gotten, component)
		})
		it('get first of two', ()=>{
			const component1 = new Component()
			const component2 = new Component()
			const components = new Components(component1, component2)
			assert.equal(components.size, 1)
			assert.deepEqual(components.get('component'), [
				component1,
				component2
			])

			const gotten = components.getComplex('component')
			assert.deepEqual(gotten, component1)
		})
		describe('array by string', ()=>{
			it('get all of non existent', ()=>{
				const components = new Components()
				assert.equal(components.size, 0)

				const gotten = components.getComplex('component$')
				assert.deepEqual(gotten, [])
			})
			it('get all of one', ()=>{
				const component = new Component()
				const components = new Components(component)
				assert.equal(components.size, 1)
				assert.deepEqual(components.get('component'), component)

				const gotten = components.getComplex('component$')
				assert.deepEqual(gotten, [component])
			})
			it('get all of two', ()=>{
				const component1 = new Component()
				const component2 = new Component()
				const components = new Components(component1, component2)
				assert.equal(components.size, 1)
				assert.deepEqual(components.get('component'), [
					component1,
					component2
				])

				const gotten = components.getComplex('component$')
				assert.deepEqual(gotten, [component1, component2])
			})
		})
		describe('array by boolean', ()=>{
			it('get all of non existent', ()=>{
				const components = new Components()
				assert.equal(components.size, 0)

				const gotten = components.getComplex('component', true)
				assert.deepEqual(gotten, [])
			})
			it('get all of one', ()=>{
				const component = new Component()
				const components = new Components(component)
				assert.equal(components.size, 1)
				assert.deepEqual(components.get('component'), component)

				const gotten = components.getComplex('component', true)
				assert.deepEqual(gotten, [component])
			})
			it('get all of two', ()=>{
				const component1 = new Component()
				const component2 = new Component()
				const components = new Components(component1, component2)
				assert.equal(components.size, 1)
				assert.deepEqual(components.get('component'), [
					component1,
					component2
				])

				const gotten = components.getComplex('component', true)
				assert.deepEqual(gotten, [component1, component2])
			})
		})
	})
})
