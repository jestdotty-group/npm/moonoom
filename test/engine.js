const assert = require('assert')

const Engine = require('../src/engine')
const Entity = require('../src/entity')

class MockSystem{
	constructor(){
		this.updated = []
	}
	update(...args){
		this.updated.push([...args])
	}
}
class MockEntity{
	has(){return this._has}
	setHas(v){this._has = v}
}
class MockQuery{
	constructor(...args){
		this.args = args
		this.searches = []
	}
	search(...args){
		this.searches.push(args)
	}
}
describe('engine', ()=>{
	describe('systems', ()=>{
		it('add', ()=>{
			const engine = new Engine()
			const system = new MockSystem()
			assert.equal(engine.systems.size, 0)

			engine.add('test', system)
			assert.equal(engine.systems.size, 1)
			assert.deepEqual(engine.systems.get('test'), [system])
		})
		it('add existing category', ()=>{
			const engine = new Engine()
			engine.add('test')
			assert.equal(engine.systems.size, 1)
			assert.deepEqual(engine.systems.get('test'), [])

			assert.throws(()=>{
				engine.add('test')
			}, new Error('System category "test" already exists'))
		})
		it('update', ()=>{
			const engine = new Engine()
			const system = new MockSystem()
			engine.add('test', (...args)=> system.update(...args))

			engine.update('test', 1, 6, 7)
			assert.deepEqual(system.updated, [
				[engine, 1, 6, 7]
			])
		})
	})
	describe('components', ()=>{
		describe('_add', ()=>{
			it('nothing', ()=>{
				const engine = new Engine()
				assert.equal(engine.components.size, 0)

				engine._add()
				assert.equal(engine.components.size, 0)
			})
			it('empty', ()=>{
				const engine = new Engine()
				assert.equal(engine.components.size, 0)

				const entity = new MockEntity()
				engine._add(entity)
				assert.equal(engine.components.size, 0)
			})
			it('one', ()=>{
				const engine = new Engine()
				assert.equal(engine.components.size, 0)

				const entity = new MockEntity()
				engine._add(entity, 'component')
				assert.equal(engine.components.size, 1)
				const set = engine.components.get('component')
				assert.deepEqual(set.size, 1)
				assert.deepEqual(set.has(entity), true)
			})
			it('duplicate', ()=>{
				const engine = new Engine()
				assert.equal(engine.components.size, 0)

				const entity = new MockEntity()
				engine._add(entity, 'component')
				assert.equal(engine.components.size, 1)
				const set = engine.components.get('component')
				assert.deepEqual(set.size, 1)
				assert.deepEqual(set.has(entity), true)

				engine._add(entity, 'component')
				assert.equal(engine.components.size, 1)
				const set2 = engine.components.get('component')
				assert.deepEqual(set2.size, 1)
				assert.deepEqual(set2.has(entity), true)
			})
		})
		describe('_remove', ()=>{
			it('nothing', ()=>{
				const engine = new Engine()
				assert.equal(engine.components.size, 0)

				engine._remove()
				assert.equal(engine.components.size, 0)
			})
			it('empty', ()=>{
				const engine = new Engine()
				assert.equal(engine.components.size, 0)

				const entity = new MockEntity()
				engine._remove(entity, false, 'component')
				assert.equal(engine.components.size, 0)
			})
			it('one', ()=>{
				const engine = new Engine()
				assert.equal(engine.components.size, 0)

				const entity = new MockEntity()
				engine._add(entity, 'component')
				assert.equal(engine.components.size, 1)
				const set = engine.components.get('component')
				assert.deepEqual(set.size, 1)
				assert.deepEqual(set.has(entity), true)

				engine._remove(entity, false, 'component')
				assert.equal(engine.components.size, 0)
			})
			it('one of duplicate', ()=>{
				const engine = new Engine()
				assert.equal(engine.components.size, 0)

				const entity = new MockEntity()
				entity.setHas(true)
				engine._add(entity, 'component', 'component')
				assert.equal(engine.components.size, 1)
				const set = engine.components.get('component')
				assert.deepEqual(set.size, 1)
				assert.deepEqual(set.has(entity), true)

				engine._remove(entity, false, 'component')
				assert.equal(engine.components.size, 1)
				const set2 = engine.components.get('component')
				assert.deepEqual(set2.size, 1)
				assert.deepEqual(set2.has(entity), true)
			})
			it('both of duplicate', ()=>{
				const engine = new Engine()
				assert.equal(engine.components.size, 0)

				const entity = new MockEntity()
				entity.setHas(true)
				engine._add(entity, 'component', 'component')
				assert.equal(engine.components.size, 1)
				const set = engine.components.get('component')
				assert.deepEqual(set.size, 1)
				assert.deepEqual(set.has(entity), true)

				engine._remove(entity, false, 'component')
				assert.equal(engine.components.size, 1)
				const set2 = engine.components.get('component')
				assert.deepEqual(set2.size, 1)
				assert.deepEqual(set2.has(entity), true)

				entity.setHas(false)
				engine._remove(entity, false, 'component')
				assert.equal(engine.components.size, 0)
			})
			it('duplicate of different entities', ()=>{
				const engine = new Engine()
				assert.equal(engine.components.size, 0)

				const entity = new MockEntity()
				engine._add(entity, 'component')
				assert.equal(engine.components.size, 1)
				const set = engine.components.get('component')
				assert.deepEqual(set.size, 1)
				assert.deepEqual(set.has(entity), true)

				const entity2 = new MockEntity()
				engine._add(entity2, 'component')
				assert.equal(engine.components.size, 1)
				const set2 = engine.components.get('component')
				assert.deepEqual(set2.size, 2)
				assert.deepEqual(set2.has(entity), true)
				assert.deepEqual(set2.has(entity2), true)

				engine._remove(entity, false, 'component')
				assert.equal(engine.components.size, 1)
				const set3 = engine.components.get('component')
				assert.deepEqual(set3.size, 1)
				assert.deepEqual(set3.has(entity2), true)

				engine._remove(entity2, false, 'component')
				assert.equal(engine.components.size, 0)
			})
		})
	})
	describe('query', ()=>{
		it('query', ()=>{
			const engine = new Engine()
			const query = new MockQuery()
			engine.query(query)
			assert.deepEqual(query.searches, [
				[engine]
			])
		})
		it('search', ()=>{
			const engine = new Engine()
			const result = engine.search(['component'])
			assert.deepEqual(result, [])
		})
		it('get', ()=>{
			const engine = new Engine()
			const result = engine.get('component')
			assert.deepEqual(result, [])
		})
	})
	describe('laziness', ()=>{
		it('create', ()=>{
			const engine = new Engine()
			const entity = engine.create(new String(), new Set())
			assert.ok(entity instanceof Entity)
			assert.equal(engine.components.size, 2)
			assert.equal(engine.components.get('string').size, 1)
			assert.equal(engine.components.get('set').size, 1)
		})
	})
})
