class Base{constructor(b){this.b=b}}
class Component{constructor(c){this.c=c}}
class Mod{constructor(m){this.m=m}}

const random = ()=> Math.floor(Math.random()*100)

module.exports = {Base, Component, Mod, random}
