const assert = require('assert')

const Query = require('../src/query')
const Engine = require('../src/engine')
const Entity = require('../src/entity')

class Component{}
class AnotherComponent{}

describe('query', ()=>{
	describe('get all', ()=>{
		it('no entities', ()=>{
			const engine = new Engine()
			assert.deepEqual(Query.getAll(engine), new Set())
		})
		it('empty entity', ()=>{
			const engine = new Engine()
			const entity = Entity.create()
			entity.attach(engine)

			assert.deepEqual(Query.getAll(engine), new Set())
		})
		it('one entity one component', ()=>{
			const engine = new Engine()
			const entity = Entity.create(new Component())
			entity.attach(engine)

			assert.deepEqual(Query.getAll(engine), new Set([entity]))
		})
		it('one entity same two components', ()=>{
			const engine = new Engine()
			const entity = Entity.create(new Component(), new Component())
			entity.attach(engine)

			assert.deepEqual(Query.getAll(engine), new Set([entity]))
		})
		it('one entity different two components', ()=>{
			const engine = new Engine()
			const entity = Entity.create(new Component(), new AnotherComponent())
			entity.attach(engine)

			assert.deepEqual(Query.getAll(engine), new Set([entity]))
		})
		it('two entities same component', ()=>{
			const engine = new Engine()
			const entities = [
				Entity.create(new Component()),
				Entity.create(new Component()),
			]
			entities.forEach(e=> e.attach(engine))

			assert.deepEqual(Query.getAll(engine), new Set([entities[0], entities[1]]))
		})
		it('two entities different components', ()=>{
			const engine = new Engine()
			const entities = [
				Entity.create(new Component()),
				Entity.create(new AnotherComponent()),
			]
			entities.forEach(e=> e.attach(engine))

			assert.deepEqual(Query.getAll(engine), new Set([entities[0], entities[1]]))
		})
	})
	describe('get specific', ()=>{
		let engine, entities
		before(()=>{
			engine = new Engine()
			entities = [
				Entity.create(new Component()),
				Entity.create(new AnotherComponent()),

				Entity.create([new Component(), new AnotherComponent()]),
				Entity.create([new Component(), new Component(), new AnotherComponent()])
			]
			entities.forEach(e=> e.attach(engine))
		})
		describe('named', ()=>{
			it('component', ()=> assert.deepEqual(Query.getSpecific(engine, 'component'), new Set([
				entities[0], entities[2], entities[3]
			])))
			it('component.anotherComponent', ()=> assert.deepEqual(Query.getSpecific(engine, 'component.anotherComponent'), new Set([
				entities[2], entities[3]
			])))
			it('anotherComponent', ()=> assert.deepEqual(Query.getSpecific(engine, 'anotherComponent'), new Set([
				entities[1]
			])))
			it('nonExistent', ()=> assert.deepEqual(Query.getSpecific(engine, 'nonExistent'), new Set()))
		})
		describe('**', ()=>{
			it('**', ()=> assert.deepEqual(Query.getSpecific(engine, '**'), new Set(entities)))
			it('**.component', ()=> assert.deepEqual(Query.getSpecific(engine, '**.component'), new Set([
				entities[0], entities[2], entities[3]
			])))
			it('**.component.anotherComponent', ()=> assert.deepEqual(Query.getSpecific(engine, '**.component.anotherComponent'), new Set([
				entities[2], entities[3]
			])))
			it('component.**.anotherComponent', ()=> assert.deepEqual(Query.getSpecific(engine, 'component.**.anotherComponent'), new Set([
				entities[2], entities[3]
			])))
		})
		describe('*', ()=>{
			it('*', ()=> assert.deepEqual(Query.getSpecific(engine, '*'), new Set(entities)))
			it('*.*', ()=> assert.deepEqual(Query.getSpecific(engine, '*.*'), new Set([
				entities[2], entities[3]
			])))
			it('*.*.*', ()=> assert.deepEqual(Query.getSpecific(engine, '*.*.*'), new Set()))
			it('*.component', ()=> assert.deepEqual(Query.getSpecific(engine, '*.component'), new Set([
				entities[3]
			])))
			it('component.*', ()=> assert.deepEqual(Query.getSpecific(engine, 'component.*'), new Set([
				entities[2], entities[3]
			])))
		})
	})
	describe('search', ()=>{
		let engine, entities
		before(()=>{
			engine = new Engine()
			entities = [
				Entity.create(new Component()),
				Entity.create(new AnotherComponent()),
				Entity.create(new Component(), new AnotherComponent()),

				Entity.create([new Component(), new AnotherComponent()]),
				Entity.create([new Component(), new Component(), new AnotherComponent()])
			]
			entities.forEach(e=> e.attach(engine))
		})
		describe('include', ()=>{
			it('nothing', ()=>{
				const query = new Query()
				assert.deepEqual(new Set(query.search(engine)), new Set(entities)) //don't maintain order so use sets
			})
			it('component', ()=>{
				const query = new Query(['component'])
				assert.deepEqual(query.search(engine), [
					entities[0], entities[2], entities[3], entities[4]
				])
			})
			it('anotherComponent', ()=>{
				const query = new Query(['anotherComponent'])
				assert.deepEqual(query.search(engine), [
					entities[1], entities[2]
				])
			})
			it('component AND anotherComponent', ()=>{
				const query = new Query(['component', 'anotherComponent'])
				assert.deepEqual(query.search(engine), [
					entities[2]
				])
			})
			it('component OR anotherComponent', ()=>{
				const query = new Query([['component', 'anotherComponent']])
				assert.deepEqual(new Set(query.search(engine)), new Set(entities))
			})
			it('component AND (anotherComponent OR component.anotherComponent)', ()=>{
				const query = new Query(['component', ['anotherComponent', 'component.anotherComponent']])
				assert.deepEqual(query.search(engine), [
					entities[2], entities[3], entities[4]
				])
			})
			it('anotherComponent OR (component.component AND component.anotherComponent)', ()=>{
				const query = new Query([['anotherComponent', ['component.component', 'component.anotherComponent']]])
				assert.deepEqual(query.search(engine), [
					entities[1], entities[2], entities[4]
				])
			})
			it('*', ()=>{
				const query = new Query(['*'])
				assert.deepEqual(new Set(query.search(engine)), new Set(entities))
			})
			it('**', ()=>{
				const query = new Query(['**'])
				assert.deepEqual(new Set(query.search(engine)), new Set(entities))
			})
		})
		it('exclude', ()=>{
			const query = new Query([], ['component'])
			assert.deepEqual(query.search(engine), [
				entities[1]
			])
		})
	})
})
